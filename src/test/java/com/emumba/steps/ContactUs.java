package com.emumba.steps;

import actors.Admin;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.waits.WaitUntil;
import ui.com.emumba.pages.home.HomePage;
import ui.com.emumba.pages.home.modals.ContactUSForm;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static ui.com.emumba.pages.home.HomeUI.ALWAYS_TRUE;
import static ui.com.emumba.pages.home.HomeUI.NAVBAR_LOGO;

public class ContactUs {
    @Given("user is on Emumba home page")
    public void userIsOnHomePage(){
        Admin.theAdmin().attemptsTo(
                HomePage.openPage(),
                WaitUntil.the(NAVBAR_LOGO, isVisible())
        );

    }
    @When("user clicks on the Contact us button")
    public void userClickOnContactUsButton(){
        Admin.theAdmin().attemptsTo(
                ContactUSForm.open()
        );
    }
    @And("user fills the contact us form")
    public void userFillsTheForm(){
        Admin.theAdmin().attemptsTo(
                ContactUSForm.fill()
        );
    }
    @Then("user should see the success message")
    public void verifyTheSuccessMessage(){
        Admin.theAdmin().attemptsTo(
                WaitUntil.the(ALWAYS_TRUE, isPresent())
        );
    }
}
