package actors;

import net.serenitybdd.annotations.Managed;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.WebDriver;

public class Admin extends Actor {
    @Managed
    WebDriver driver;
    public Admin() {
        super("Admin");
        this.whoCan(BrowseTheWeb.with(this.driver));

    }
    public static Actor theAdmin(){
        return new Admin();
    }

}
