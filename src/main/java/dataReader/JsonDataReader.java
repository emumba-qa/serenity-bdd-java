package dataReader;

import com.google.gson.JsonObject;
import jsonutils.JsonUtils;

import java.io.IOException;

public class JsonDataReader {
    private static final String CONTACT_US = "Contact_US";
    private static final String FILE_PATH = "src/test/resources/web/testdata/contact_us.json";
    private static final JsonObject jsonObject;

    static {
        try {
            jsonObject = JsonUtils.parseJsonFile(FILE_PATH);
        } catch (IOException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static String getName() {
        return JsonUtils.getString(jsonObject
                .getAsJsonArray(CONTACT_US)
                .get(0).getAsJsonObject(), "Name");

    }

    public static String getEmail() {
        return JsonUtils.getString(jsonObject
                .getAsJsonArray(CONTACT_US)
                .get(0).getAsJsonObject(), "Email");
    }

    public static String getMessage() {
        return JsonUtils.getString(jsonObject
                .getAsJsonArray(CONTACT_US)
                .get(0).getAsJsonObject(), "Message");
    }
}
