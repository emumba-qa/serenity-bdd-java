package jsonutils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.FileReader;
import java.io.IOException;

public class JsonUtils {
    private static final Gson gson = new Gson();
    private static final JsonParser parser = new JsonParser();

    public static JsonObject parseJsonFile(String filePath) throws IOException {
        try (FileReader reader = new FileReader(filePath)) {
            JsonElement rootElement = parser.parse(reader);
            return rootElement.getAsJsonObject();
        }
    }

    public static JsonArray getJsonArray(JsonObject jsonObject, String key) {
        return jsonObject.getAsJsonArray(key);
    }

    public static JsonObject getJsonObject(JsonObject jsonObject, String key) {
        return jsonObject.getAsJsonObject(key);
    }

    public static String getString(JsonObject jsonObject, String key) {
        return jsonObject.get(key).getAsString();
    }
}
